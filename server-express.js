const Joi =  require('joi');
const config = require("config");
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const courses = require('./routes/courses');
const home = require('./routes/home');
const express = require("express");
const app = express();
//we dont need require for pug ; express will internally load the pug module
app.set('view engine', 'pug');
app.set('views',path.join(__dirname, 'views')); //folder views
app.use(express.json());
//app.use(morgan('combined')); wil log in the console by default

//pentru fiecare url care incepe cu api/courses ii spunem lui express sa foloseasca routerul dat de noi (courses)
app.use('/api/courses',courses);
app.use('/',home);

let accessLogStream = fs.createWriteStream(path.join(__dirname,'access.log'),{flags:'a'})
app.use(morgan('combined',{stream:accessLogStream}));


//configuration
console.log('Application name: '+config.get('name'));
console.log('Application name: '+config.get('mail.host'));

//daca nu dam portul din consola  (set PORT=XXXX, se va pune 3000)
const port = process.env.PORT || 3000;
app.listen(port,()=>console.log(`Listening on port ${port}...`));