
//fiind scris cu litere mari =>  E O CLASA
const EventEmitter = require('events');
//TREBUIE SA CREAM O INSTANTA A CLASEI
const emitter = new EventEmitter();

//trebuie sa stea inainte de emit !! deoare cand se emite eventul, emitter cauta prin lista de listeneri si ii apeleaza synchronously
//trebuie sa inregistram un listener care sa asculte pe coada de evenimente
emitter.on("messageLogged", function(eventArg){
    console.log('Listener called',eventArg);
})
//emit() -> raise an event
//emit ->making a noise, produce something
emitter.emit('messageLogged',{id:1, url:'diana.com'});

var _ = require('underscore');
console.log(_.contains([12,4,6],4));

/*
EcmaScript6 -> we have arrow function
emmiter.on('messageLogged', (arg)=> {

});
 */










//const files = fs.readdirSync('./');
//console.log(files);